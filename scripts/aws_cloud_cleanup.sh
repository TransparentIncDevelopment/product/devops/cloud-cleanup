#!/bin/bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

PREPEND="sgd-"
PROJECT="xand-qa"
REGIONS=$(aws ec2 describe-regions --all-regions --query "Regions[].{Name:RegionName}" --output text)

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by automation.
    The purpose of this script is to delete AWS SGD resources with a specific prepended name on a schedule in CI/CD.
END
)
echo "$HELPTEXT"
}

function error {
    echo "$@"
    echo
    echo "$(helptext)"
    exit 1
}

function info () {
    local green="\033[32m"
    local normal="\033[00m"
    echo -e "${green}$1${normal}"
}

### Store EIP allocation ID for NAT
info "finding NAT EIP to store for release..."
nat_eip=$(aws ec2 describe-nat-gateways --no-cli-pager --query "NatGateways[].NatGatewayAddresses[].AllocationId" --filter "Name=tag:Name,Values=$PREPEND*" --output text)

### Delete NATs
info "Finding NATs..."
nats=$(aws ec2 describe-nat-gateways --no-cli-pager --query "NatGateways[].NatGatewayId" --filter "Name=tag:Name,Values=$PREPEND*" --output text)
for nat in $nats
do
  echo "Deleting $nat"
  aws ec2 delete-nat-gateway --nat-gateway-id $nat --no-cli-pager
#TODO Make some kind of delay or polling for NAT to fully delete
  echo "Sleeping to allow for deletion of NAT..."
  sleep 1m
done
if [[ -z "$nats" ]]
then
  info "No NATs found."
fi

### Delete ELBv2s

#aws elbv2 operates on the arn and not the ID so that's what we'll use
info "Finding Elastic Load Balancers v2..."
load_balancers=$(aws elbv2 describe-load-balancers --no-cli-pager --query "LoadBalancers[].LoadBalancerArn" --output text)
for load_balancer_arn in $load_balancers
do
  load_balancer_tags=$(aws elbv2 describe-tags --resource-arns "$load_balancer_arn" --no-cli-pager --output json)
  echo $load_balancer_tags

# Delete Load Balancers... aws cli does not support --query so we must use jq from the json output. Passing bash variables is done with env.VARIABLE. jq -e is used to provide output as exit status.

#TODO I'm sure there's a simpler jq query, but this works.
  if $(echo $load_balancer_tags | jq -e '.TagDescriptions[].Tags[] | select(.Key | startswith("kubernetes.io/cluster/"+env.PREPEND) ) | any(startswith("kubernetes.io/cluster/"+env.PREPEND))')
  then
    aws elbv2 delete-load-balancer --load-balancer-arn $load_balancer_arn
    info "$load_balancer_arn deleted"
  fi
done

### Delete EKS cluster
info "Finding EKS cluster..."
clusters=$(aws eks list-clusters --no-cli-pager --query "clusters[]" --output text)
for cluster in $clusters
do
  if [[ $cluster == $PREPEND* ]]
  then
    info "Deleting cluster $cluster"
    aws eks delete-cluster --name $cluster --no-cli-pager
  fi
done

### Delete Auto Scaling Groups
info "Finding Auto Scaling Groups..."
auto_scaling_groups=$(aws autoscaling describe-auto-scaling-groups --no-cli-pager --query "AutoScalingGroups[].AutoScalingGroupName" --output text)
for auto_scaling_group in $auto_scaling_groups
do
  if [[ $auto_scaling_group == $PREPEND* ]]
  then
    info "Deleting Auto Scaling Group $auto_scaling_group"
    aws autoscaling delete-auto-scaling-group --auto-scaling-group-name $auto_scaling_group --force-delete --no-cli-pager
#TODO implement polling rather than a sleep
    info "Sleeping 5 minutes to allow for deletion of Auto Scaling Group"
    sleep 5m
  fi
done

### Wait for Auto Scaling Group to die

### Deleting subnets
info "Finding Subnets..."
subnets=$(aws ec2 describe-subnets --filters "Name=tag:Name,Values=$PREPEND*" --query "Subnets[].SubnetId" --output text)
for subnet in $subnets
do
  info "Deleting Subnet $subnet"
  aws ec2 delete-subnet --subnet-id $subnet --no-cli-pager
done

### Deleting Route Tables
info "Finding Route Tables..."
route_tables=$(aws ec2 describe-route-tables --filters "Name=tag:Name,Values=$PREPEND*" --query "RouteTables[].RouteTableId"  --output text)
for route_table in $route_tables
do
## You can't delete the "main" route_table
  if [[ $(aws ec2 describe-route-tables --route-table-ids $route_table --query "RouteTables[].Associations[].Main"  --output text --output text) != "true" ]]
  then
    info "Deleting Route Table $route_table"
    aws ec2 delete-route-table --route-table-id $route_table --no-cli-pager
  fi
done

### Delete Network ACLs
info "Finding Network ACLs..."
### You can't delete the default Network ACL
network_acls=$(aws ec2 describe-network-acls --filters "Name=tag:Name,Values=$PREPEND*" "Name=default,Values=false" --query "NetworkAcls[].NetworkAclId" --output text)
for network_acl in $network_acls
do
  info "deleting ACL $network_acl"
  aws ec2 delete-network-acl --network-acl-id $network_acl --no-cli-pager
done

### Security groups that reference another security group cannot be deleted, so remove all rules from the security groups before deleting them

### Delete Security Groups
info "Finding Security Groups..."
  security_groups=$(aws ec2 describe-security-groups --filters "Name=tag:Name,Values=$PREPEND*" --query "SecurityGroups[].GroupId" --output text)
for security_group in $security_groups
do
### You can't delete the default security group
  if [[ $(aws ec2 describe-security-groups --group-ids $security_group --query "SecurityGroups[].GroupName" --output text) != "default" ]]
  then    
    unset ingress_rules
    unset egress_rules
    if [[ $(aws ec2 describe-security-groups --output json --group-ids $security_group --query "SecurityGroups[0].IpPermissions") == "[]" ]]
    then
      info "no security groups found"
    else
      info "removing ingress rules from $security_group"
      aws ec2 revoke-security-group-ingress --group-id $security_group --ip-permissions "$(aws ec2 describe-security-groups --output json --group-ids $security_group --query "SecurityGroups[0].IpPermissions")" --no-cli-pager --output text
    fi

    if [[ $(aws ec2 describe-security-groups --output json --group-ids $security_group --query "SecurityGroups[0].IpPermissionsEgress") == "[]" ]]
    then
      info "no security groups found" 
    else
      info "removing egress rules from $security_group"
      aws ec2 revoke-security-group-egress --group-id $security_group --ip-permissions "$(aws ec2 describe-security-groups --output json --group-ids $security_group --query "SecurityGroups[0].IpPermissionsEgress")" --no-cli-pager --output text
    fi
  fi
  done

#delete the security groups
for security_group in $security_groups
  do
    ### You can't delete the default security group
    if [[ $(aws ec2 describe-security-groups --group-ids $security_group --query "SecurityGroups[].GroupName" --output text) != "default" ]]
    then    
      info "Deleting Security Group $security_group"
      aws ec2 delete-security-group --group-id $security_group --no-cli-pager
    fi
  done

### Delete resources where we must know VPC ###

### Delete Internet Gateways

info "Finding Internet Gateways and VPCs..."
vpcs=$(aws ec2 describe-vpcs --no-cli-pager --filters "Name=tag:Name,Values=$PREPEND*" --query "Vpcs[].VpcId" --output text)
for vpc in $vpcs
do

  internet_gateways=$(aws ec2 describe-internet-gateways --no-cli-pager --filters "Name=attachment.vpc-id,Values=$vpc" --query "InternetGateways[].InternetGatewayId" --output text)
  for internet_gateway in $internet_gateways
  do
    info "deleting Internet Gateway $internet_gateway"
    aws ec2 detach-internet-gateway --internet-gateway-id $internet_gateway --vpc-id $vpc
    aws ec2 delete-internet-gateway --internet-gateway-id $internet_gateway --no-cli-pager 
  done

### Delete VPCs

  info "Deleting $vpc"
  aws ec2 delete-vpc --vpc-id $vpc --no-cli-pager
done

### Release NAT EIP
if [[ -z $nat_eip ]]
  then
    info "No EIP stored to release."
  else
  for eip in $nat_eip
  do
    if aws ec2 describe-addresses --allocation-ids $eip > /dev/null 2>&1 ;
    then 
      info "Releasing $eip"
      aws ec2 release-address --allocation-id $eip
    else
      info "Stored EIP doesn't exist."
    fi
  done
fi

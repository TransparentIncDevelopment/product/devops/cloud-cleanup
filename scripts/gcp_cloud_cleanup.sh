#!/bin/bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

PREPEND="sgd-"
PROJECT="xand-qa"
ZONES=$(gcloud compute zones list --format="value(name.scope())") 
REGIONS=$(gcloud compute regions list --format="value(name.scope())")

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by automation.
    The purpose of this script is to delete GCP SGD resources with a specific prepended name on a schedule in CI/CD.
END
)
echo "$HELPTEXT"
}

function error {
    echo "$@"
    echo
    echo "$(helptext)"
    exit 1
}

function info () {
    local green="\033[32m"
    local normal="\033[00m"
    echo -e "${green}$1${normal}"
}

##TODO Find region logic

### Delete NAT (per router)
info "Finding NATs..."
for router in $(gcloud compute routers list --verbosity=error --project=$PROJECT --format="value(name.scope())" --filter="name~^$PREPEND")
do
  for nat in $(gcloud compute routers nats list --verbosity=error --project=$PROJECT --format="value(name.scope())" --router-region="$(gcloud compute routers list --verbosity=error --project=$PROJECT --format="value(region.scope())" --filter="name=$router")" --router=$router)
  do
    info "Deleting $nat"
    gcloud compute routers nats delete $nat -q --region=$(gcloud compute routers list --verbosity=error --project=$PROJECT --format="value(region.scope())" --filter="name=$router") --router=$router --project=$PROJECT
  done
done

### Delete Cloud Routers (per region)
info "Finding cloud routers..."
for region in $REGIONS
do
  for router in $(gcloud compute routers list --verbosity=error --project=$PROJECT --format="value(name.scope())" --filter="name~^$PREPEND AND region:$region")
  do
    info "Deleting $router"
    gcloud compute routers delete $router -q --region=$region --project=$PROJECT
  done
done

### Delete k8s clusters (per zone)
info "Finding k8s clusters..."
for zone in $ZONES
do
  for cluster in $(gcloud container clusters list --verbosity=error --zone=$zone --project=$PROJECT --format="value(name.scope())" --filter="name~^$PREPEND AND zone:$zone")
  do
    info "Deleting $cluster"
    gcloud container clusters delete $cluster -q --verbosity=error --zone=$zone --project=$PROJECT
  done
done

### Delete subnets (per VPC)
info "Finding subnets..."
for vpc in $(gcloud compute networks list --verbosity=error --project=$PROJECT --format="value(name.scope())" --filter="name~^$PREPEND")
do
  for subnet in $(gcloud compute networks subnets list --network=$vpc --project=$PROJECT --format="value(name.scope())")
  do
    info "Deleting $subnet"
    gcloud compute networks subnets delete $subnet -q --region=$(gcloud compute networks subnets list --verbosity=error --project=$PROJECT --format="value(region.scope())" --filter="name=$subnet") --project=$PROJECT
  done
done

### Delete VPC (per project)
info "Finding VPCs..."
for vpc in $(gcloud compute networks list --verbosity=error --project=$PROJECT --format="value(name.scope())" --filter="name~^$PREPEND")
do
  info "Deleting $vpc..."
  gcloud compute networks delete $vpc -q --project=$PROJECT
done

### Delete pvc disks (per zone)
info "Finding PVC disks..."
for zone in $ZONES
do
  for disk in $(gcloud compute disks list --verbosity=error --project=$PROJECT --format="value(name.scope())" --filter="name~^gke-$PREPEND.*-pvc-.* AND zone:$zone")
  do
    info "Deleting $disk"
    gcloud compute disks delete $disk -q --verbosity=error --zone=$zone --project=$PROJECT
  done
done

### Release External IP
info "Finding external IPs..."
for ip in $(gcloud compute addresses list --verbosity=error --project=$PROJECT --format="value(name.scope())" --filter="name~^xand-nat-manual-ip-$PREPEND.* AND addressType:EXTERNAL AND status:RESERVED")
do
  info "Releasing $ip"
  gcloud compute addresses delete $ip -q --region="$(gcloud compute addresses list --verbosity=error --project=$PROJECT --format="value(region.scope())" --filter="name:$ip AND addressType:EXTERNAL AND status:RESERVED")" --project=$PROJECT

done

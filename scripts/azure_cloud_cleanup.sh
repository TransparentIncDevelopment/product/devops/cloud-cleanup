#!/bin/bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

PREPEND="sgd"

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by automation.
    The purpose of this script is to delete Azure SGD resources with a specific prepended name on a schedule in CI/CD.
END
)
echo "$HELPTEXT"
}

function error {
    echo "$@"
    echo
    echo "$(helptext)"
    exit 1
}

function info () {
    local green="\033[32m"
    local normal="\033[00m"
    echo -e "${green}$1${normal}"
}

### In Azure, Resource Groups contain everything, so delete the two created for each deployment and everything else gets deleted.

### Delete Cluster Resource Group
info "Finding Cluster Resource Group..."
for cluster_resource_group in $(az group list --query "[].name" --output tsv)
do
  if [[ $cluster_resource_group == "xand-"$PREPEND* ]]
  then
    info "Deleting $cluster_resource_group"
    az group delete --name $cluster_resource_group -y
  fi
done

### Delete Bucket Resource Group
info "Finding Bucket Resource Group..."
for bucket_resource_group in $(az group list --query "[].name" --output tsv)
do
  if [[ $bucket_resource_group == $PREPEND* ]]
  then
    info "Deleting $bucket_resource_group"
    az group delete --name $bucket_resource_group -y
  fi
done
